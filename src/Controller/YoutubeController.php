<?php

namespace App\Controller;

use App\Entity\Youtube;
use App\Form\YoutubeType;
use App\Repository\YoutubeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class YoutubeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function home(Request $req, EntityManagerInterface $em, YoutubeRepository $repo): Response
    {
        $youtube = new Youtube();
        $form = $this->createForm(YoutubeType::class, $youtube);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $youtube = $form->getData();
            $em->persist($youtube);
            $em->flush();
            return $this->redirectToRoute('app_home');
        }
        return $this->render('youtube/index.html.twig', [
            'form' => $form->createView(), 'videos' => $repo->findAll()
        ]);
    }


    /**
     * @Route("/{id}", name="app_video")
     */
    public function video(Youtube $video): Response
    {
        return $this->render('youtube/video.html.twig', [
            'videoName' => $video->getName(),
            'videoUrl' => $video->getUrl(),
        ]);
    }
}
